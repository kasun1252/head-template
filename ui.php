<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Template</title>
        <link href="#" rel="shortcut icon">
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
        <link rel="stylesheet" type="text/css" href="styles/css/component.css" />
        <link rel="stylesheet" type="text/css" href="styles/css/loading.css" />
        <script type="text/javascript" src="js/dist/main.min.js"></script>
        <script type="text/javascript" src="js/js-main/jquery-1.12.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        <header class="headerTest1">
            <div class="header-content">
                <div class="header-logo">
                    <a href="#">
                        <img src="img/html5.png" alt="logo">
                    </a>
                </div>
                <div class="navi-bar">
                    <div class="content-bar">
                        <div class="nav-box">
                            <p>Change will change here</p>
                        </div>
                        <div class="nav-box">
                            <p>Change will change here</p>
                        </div>
                        <div class="nav-box">
                            <p>Change will change here</p>
                        </div>
                    </div>
                    <div class="navi-main">
                        <nav>
                            <ul>
                                <li id="home">
                                    <a href="#">home</a>
                                    <ul class="sublinks">
                                        <li><a href="">Link1</a></li>
                                        <li><a href="">Link2</a></li>
                                        <li><a href="">Link3</a></li>
                                        <li><a href="">Link4</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Downloads</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">About us</a></li>

                                <script>
                                  // $('document').ready(function(){
                                  //   $('ul.sublinks').hode();
                                  //   $('#home').click(function(){

                                  //   });
                                  // });
                                </script>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div class="body-section">
            <div class="row">
                <div class="container">
                    <div class="content-wrap drag-box">
                        <div class="dragbox-wrap">
                            <div id="bubble" class="box1"></div>
                            <div class="box2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>