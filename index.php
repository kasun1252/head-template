<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="#" rel="shortcut icon">
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
        <link rel="stylesheet" type="text/css" href="styles/css/component.css" />
        <link rel="stylesheet" type="text/css" href="styles/css/loading.css" />
        <script type="text/javascript" src="js/dist/main.min.js"></script>
        <script type="text/javascript" src="js/js-main/jquery-1.12.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        <header class="headerTest1">
            <div class="header-content">
                <div class="header-logo">
                    <a href="#">
                        <img src="img/html5.png" alt="logo">
                    </a>
                </div>
                <div class="navi-bar">
                    <div class="content-bar">
                        <div class="nav-box">
                            <p>Lorem ipsum</p>
                        </div>
                        <div class="nav-box">
                            <p>Lorem ipsum</p>
                        </div>
                        <div class="nav-box">
                            <p>Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="navi-main">
                        <button id="mainNav">Click</button>
                        <nav class="navigation">
                            <ul id="myDropdown" class="dropdown-content">
                                <li>
                                    <a href="#">home</a>
                                    <ul>
                                        <li><a href="">Link1</a></li>
                                        <li><a href="">Link2</a></li>
                                        <li><a href="">Link3</a></li>
                                        <li><a href="">Link4</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Downloads</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">About us</a></li>
                            </ul>
                        </nav>
                    </div>

                </div>
            </div>

            <script>

                $(document).ready(function(){
                    //$('.dropdown-content').hide();

                    $('#mainNav').click(function(){
                        $(".dropdown-content").slideToggle("slow");
                    });
                });
                
            </script>

        </header>
        <header class="headerTest2">
            <div class="header-content">
                <div class="header-logo">
                    <a href="">
                        <img src="img/html5.png" alt="logo">
                    </a>
                </div>
                <div class="navi-bar">
                    <nav>
                        <ul>
                            <li>
                                <a href="#">home</a>
                            </li>
                            <li>
                                <a href="#">Services</a>
                            </li>
                            <li>
                                <a href="#">Downloads</a>
                            </li>
                            <li>
                                <a href="#">Community</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        </script>
        <div class="body-section">
            <div class="row">
                <div class="container">
                    <div class="content-wrap test">
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        
    </script>
</html>