$(document).ready(function() {
	var btn_clk = true;
    
    $("#btn_submit").click(function () {

        if(btn_clk)
        {
        	$(".response-overlay").show();
            btn_clk = false;
            var input = new Object();
            input.conname = $("input[name=conname]").val();
            input.conemail = $("input[name=conemail]").val();
            input.conphone = $("input[name=conphone]").val();
            input.conmessage = $("textarea[name=conmessage]").val();
            $("#conname, #conemail, #conphone").css({"border-bottom-color":"#383838"});
            $(".error_msg").html('');
            //client side validation
            var form_validation = true;
            if(input.conname == '' || input.conname == null)
            {
                form_validation = false;
                $("#error_conname").html('*Please tell us your name');
                $("#conname").css({"border-color":"#ff5656"});
            }
            if(input.conemail == '' || input.conemail == null)
            {
                form_validation = false;
                $("#error_conemail").html('*Please tell us your email');
                $("#conemail").css({"border-bottom-color":"#ff5656"});
            } 
            else if (! validateEmail(input.conemail))
            {
                form_validation = false;
                $("#error_conemail").html('*Please enter a valid email');
                $("#conemail").css({"border-color":"#ff5656"});
            }

            if(input.conphone == '' || input.conphone == null)
            {
                form_validation = false;
                $("#error_conphone").html('*Please tell us your phone number');
                $("#conphone").css({"border-color":"#ff5656"});
            }

            else if (! validatePhone(input.conphone))
            {
                form_validation = false;
                $("#error_conphone").html('*Please enter a valid phone');
                $("#conphone").css({"border-color":"#ff5656"});
            }
            
            if(form_validation)
            {
                $.post("./inc/contact.php",input,function(output){
                    obj = JSON.parse(output);
                    if(obj == 1)
                    {
                        $(".response-content").show();
                        $('.resopons-msg').html('<h2>Thank you!</h2> <h5>You are very important to us, We will contact you very soon...</h5>');
                    	setTimeout(function(){ 
                    		location.reload();
                    	 }, 3500);
                    }
                    else
                    {
                        $(".response-content").show();
                        $('.resopons-msg').html("<h2 style='color:#cc170a'>Error!</h2> <h5 style='color:#9c2222'>Server response error...!</h5>");
                        btn_clk= true;
                        setTimeout(function(){ 
                    		$(".response-overlay").hide();
                    	 }, 2500);
                    }
                });
            }
            else
            {
            	$(".response-overlay").hide();
                btn_clk = true;
            }
        }
     return false;
    });
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    var re = /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/;
    return re.test(phone);
}
