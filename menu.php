<html>
    <head>
        <meta charset="UTF-8">
        <title>Template</title>
        <link href="#" rel="shortcut icon">
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
        <link rel="stylesheet" type="text/css" href="styles/css/component.css" />
        <link rel="stylesheet" type="text/css" href="styles/css/loading.css" />
        <script type="text/javascript" src="js/dist/main.min.js"></script>
        <script type="text/javascript" src="js/js-main/jquery-1.12.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $(".menu-toggle").on('click', function() {
                $(this).toggleClass("on");
                $('.menu-section').toggleClass("on");
                $("nav ul").toggleClass('hidden');
            });
        </script>

        <style>
            .menu-toggle {
              width: 40px;
              height: 30px;
              position: absolute;
              top: 20px;
              right: 25px;
              cursor: pointer;
              
              &.on {
                .one {
                  @include transform(rotate(45deg) translate(7px, 7px));
                }

                .two {
                  opacity: 0;
                }

                .three {
                  @include transform (rotate(-45deg) translate(8px, -10px));
                }
              }
            }

            .one,
            .two,
            .three{
              width: 100%;
              height: 5px;
              background: white;
              margin: 6px auto;
              backface-visibility: hidden;
              @include transition-duration(0.3s);
            }

            nav ul{
              margin: 0;
              padding: 0;
              font-family: Open Sans;
              list-style: none;
              margin: 4em auto;
              text-align: center;
              
              &.hidden {
                display: none;
              }
                
                a {
                @include transition-duration(0.5s);
                text-decoration: none;
                color: white;
                font-size: 3em;
                line-height: 1.5;
                width: 100%;
                display: block;
                  &:hover {
                    background-color: rgba(0,0,0,0.5);
                  }
                }
            }

            .menu-section {
              &.on {
              z-index: 10;
              width: 100%;
              height: 100%;
              display: block;
              background-color: rgba(0,0,0,0.5);
              position: absolute;
              }
            }
        </style>

    </head>

    <body>
        <div class="menu-section">
          <div class="menu-toggle">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
          </div>
          <nav>
                <ul role="navigation" class="hidden">
                    <li><a href="#">work</a></li>
                    <li><a href="#">about</a></li>
                    <li><a href="#">resume</a></li>
                    <li><a href="#">contact</a></li>
                </ul>
            </nav>
        </div>
    </body>

</html>